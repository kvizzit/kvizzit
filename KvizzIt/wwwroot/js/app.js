$('document').ready(function() {
  var isVerified = false;
  var userProfile;
  $('adminView').hide();

  var webAuth = new auth0.WebAuth({
    domain: 'kvizzit.eu.auth0.com',
    clientID: 'Ktk9jpiIEYtzbqpJV5O7ifwtPlSXjIER',
    redirectUri: 'https://kvizzit.com/',
    audience: 'https://' + 'kvizzit.eu.auth0.com' + '/userinfo',
    responseType: 'token id_token',
    scope: 'openid profile read:messages write:messages',
  });

  var options = {
    theme: {
      logo: 'https://i.imgur.com/TwmNXs8.png',
      primaryColor: '#31324F'
    }
  };

  var loginBtn = $('#btn-login');

  loginBtn.click(function(e) {
    e.preventDefault();
    webAuth.authorize();
    userVerification();
  });

  var loginStatus = $('.container h4');
  var loginView = $('#login-view');
  var homeView = $('#home-view');

  // buttons and event listeners
  var homeViewBtn = $('#btn-home-view');
  var loginBtn = $('#btn-login');
  var logoutBtn = $('#btn-logout');

  homeViewBtn.click(function() {
    homeView.css('display', 'inline-block');
    loginView.css('display', 'none');
  });

  loginBtn.click(function(e) {
    e.preventDefault();
    webAuth.authorize();
  });

  logoutBtn.click(logout);

  function setSession(authResult) {
    const scopes = authResult.scope || requestedScopes || '';
    // Set the time that the Access Token will expire at
    var expiresAt = JSON.stringify(
      authResult.expiresIn * 1000 + new Date().getTime()
    );
    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', expiresAt);
    localStorage.setItem('scopes', JSON.stringify(scopes));
    getProfile();
  }

  function logout() {
    // Remove tokens and expiry time from localStorage
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    displayButtons();
  }

  function isAuthenticated() {
    // Check whether the current time is past the
    // Access Token's expiry time
    var expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    return new Date().getTime() < expiresAt;
  }

  function handleAuthentication() {
    webAuth.parseHash(function(err, authResult) {
      if (authResult && authResult.accessToken && authResult.idToken) {
        window.location.hash = '';
        setSession(authResult);
        loginBtn.css('display', 'none');
        homeView.css('display', 'inline-block');
      } else if (err) {
        homeView.css('display', 'inline-block');
        console.log(err);
        alert(
          'Error: ' + err.error + '. Check the console for further details.'
        );
      }
      displayButtons();
    });
  }

  function displayButtons() {
    if (isAuthenticated()) {
      loginBtn.css('display', 'none');
      logoutBtn.css('display', 'inline-block');
      $("#loggedIn").show();
      loginStatus.text('You are logged in!');

    } else {
      loginBtn.css('display', 'inline-block');
      logoutBtn.css('display', 'none');
      $("#loggedIn").hide();
      loginStatus.text('You are not logged in! Please log in to continue.');
    }

    if (!isAuthenticated()) {
      $(".adminView").hide();
    } else {
      $(".adminView").show();
    }
  }

  

  function getProfile() {
    if (!userProfile) {
      var accessToken = localStorage.getItem('access_token');

      if (!accessToken) {
        console.log('Access Token must exist to fetch profile');
      }

      webAuth.client.userInfo(accessToken, function(err, profile) {
        if (profile) {
          userProfile = profile;
          displayProfile();
          userProfile.app_metadata = userProfile.app_metadata || {};
          if (userProfile.app_metadata.roles.indexOf('admin')){
            console.log("Is Admin");
          }
        }
      });
    } else {
      displayProfile();
    }
  }

  function userHasScopes(scopes) {
    var savedScopes = JSON.parse(localStorage.getItem('scopes'));
    if (!savedScopes) return false;
    var grantedScopes = savedScopes.split(' ');
    for (var i = 0; i < scopes.length; i++) {
      if (grantedScopes.indexOf(scopes[i]) < 0) {
        return false;
      }
    }
    return true;
  }

  function displayProfile() {
    // display the profile
    $('#profile-view .nickname').text(userProfile.nickname);
    $('#profile-view .full-profile').text(JSON.stringify(userProfile, null, 2));
    $('#profile-view img').attr('src', userProfile.picture);
  }

  function userVerification(user, context, callback) {
    if (!user.email_verified) {
      isVerified = false;
      return callback(new UnauthorizedError('Please verify your email before logging in.'));
      
    } else {
      isVerified = true;
      return callback(null, user, context);
    }
  }

  handleAuthentication();

});