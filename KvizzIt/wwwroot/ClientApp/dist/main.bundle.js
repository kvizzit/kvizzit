webpackJsonp(["main"],{

/***/ "./ClientApp/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./ClientApp/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./ClientApp/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./ClientApp/app/app.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var dataService_1 = __webpack_require__("./ClientApp/app/shared/dataService.ts");
var AppComponent = /** @class */ (function () {
    function AppComponent(elementRef, data) {
        this.elementRef = elementRef;
        this.data = data;
        this.title = 'KvizzIt';
        this.quizId = this.elementRef.nativeElement.getAttribute('quizId');
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.data.currentMessage.subscribe(function (message) { return _this.message = message; });
        this.data.changeMessage(this.quizId);
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            template: __webpack_require__("./ClientApp/app/app.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [core_1.ElementRef, dataService_1.DataService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;


/***/ }),

/***/ "./ClientApp/app/app.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var forms_1 = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
var ng2_dnd_1 = __webpack_require__("./node_modules/ng2-dnd/ng2-dnd.es5.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var common_1 = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var app_component_1 = __webpack_require__("./ClientApp/app/app.component.ts");
var dataService_1 = __webpack_require__("./ClientApp/app/shared/dataService.ts");
var question_component_1 = __webpack_require__("./ClientApp/question/question.component.ts");
var question_service_1 = __webpack_require__("./ClientApp/question/question.service.ts");
var takequiz_component_1 = __webpack_require__("./ClientApp/app/takequiz/takequiz.component.ts");
var not_found_component_1 = __webpack_require__("./ClientApp/app/not-found.component.ts");
var browse_component_1 = __webpack_require__("./ClientApp/app/browse/browse.component.ts");
var appRoutes = [
    { path: 'Home/CreateQuiz', component: question_component_1.Question },
    { path: 'Home/TakeQuiz/:id', component: takequiz_component_1.TakequizComponent },
    { path: 'Home/Browse', component: browse_component_1.BrowseComponent },
    { path: '**', component: not_found_component_1.PageNotFoundComponent }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                question_component_1.Question,
                takequiz_component_1.TakequizComponent,
                not_found_component_1.PageNotFoundComponent,
                browse_component_1.BrowseComponent
            ],
            imports: [
                platform_browser_1.BrowserModule,
                http_1.HttpClientModule,
                forms_1.FormsModule,
                ng2_dnd_1.DndModule.forRoot(),
                router_1.RouterModule.forRoot(appRoutes
                /*       { enableTracing: true} // logger alle interne routing events til consollen */
                )
            ],
            providers: [
                dataService_1.DataService,
                question_service_1.QuestionService,
                common_1.DatePipe,
                { provide: common_1.APP_BASE_HREF, useValue: "/" }
            ],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;


/***/ }),

/***/ "./ClientApp/app/browse/browse.component.css":
/***/ (function(module, exports) {

module.exports = ".spinner {\r\n    margin:  300px auto 300px auto;\r\n    width: 40px;\r\n    height: 40px;\r\n    position: relative;\r\n  }\r\n  \r\n  .cube1, .cube2 {\r\n    background-color: #333;\r\n    width: 15px;\r\n    height: 15px;\r\n    position: absolute;\r\n    top: 0;\r\n    left: 0;\r\n    \r\n    -webkit-animation: sk-cubemove 1.8s infinite ease-in-out;\r\n    animation: sk-cubemove 1.8s infinite ease-in-out;\r\n  }\r\n  \r\n  .cube2 {\r\n    -webkit-animation-delay: -0.9s;\r\n    animation-delay: -0.9s;\r\n  }\r\n  \r\n  @-webkit-keyframes sk-cubemove {\r\n    25% { -webkit-transform: translateX(42px) rotate(-90deg) scale(0.5) }\r\n    50% { -webkit-transform: translateX(42px) translateY(42px) rotate(-180deg) }\r\n    75% { -webkit-transform: translateX(0px) translateY(42px) rotate(-270deg) scale(0.5) }\r\n    100% { -webkit-transform: rotate(-360deg) }\r\n  }\r\n  \r\n  @keyframes sk-cubemove {\r\n    25% { \r\n      transform: translateX(42px) rotate(-90deg) scale(0.5);\r\n      -webkit-transform: translateX(42px) rotate(-90deg) scale(0.5);\r\n    } 50% { \r\n      transform: translateX(42px) translateY(42px) rotate(-179deg);\r\n      -webkit-transform: translateX(42px) translateY(42px) rotate(-179deg);\r\n    } 50.1% { \r\n      transform: translateX(42px) translateY(42px) rotate(-180deg);\r\n      -webkit-transform: translateX(42px) translateY(42px) rotate(-180deg);\r\n    } 75% { \r\n      transform: translateX(0px) translateY(42px) rotate(-270deg) scale(0.5);\r\n      -webkit-transform: translateX(0px) translateY(42px) rotate(-270deg) scale(0.5);\r\n    } 100% { \r\n      transform: rotate(-360deg);\r\n      -webkit-transform: rotate(-360deg);\r\n    }\r\n  }"

/***/ }),

/***/ "./ClientApp/app/browse/browse.component.html":
/***/ (function(module, exports) {

module.exports = "<p>{{listFilter}}</p>\r\n<div class=\"sidenav\">\r\n    <a (click)=\"filter('Animals')\">Animals</a>\r\n    <a (click)=\"filter('Culture')\">Culture</a>\r\n    <a (click)=\"filter('Film/TV')\">Film/TV</a>\r\n    <a (click)=\"filter('Food and drink')\">Food and drink</a>\r\n    <a (click)=\"filter('Geography')\">Geography</a>\r\n    <a (click)=\"filter('History')\">History</a>\r\n    <a (click)=\"filter('Motors')\">Motors</a>\r\n    <a (click)=\"filter('Movies')\">Movies</a>\r\n    <a (click)=\"filter('Music')\">Music</a>\r\n    <a (click)=\"filter('People')\">People</a>\r\n    <a (click)=\"filter('Politics')\">Politics</a>\r\n    <a (click)=\"filter('Religion')\">Religion</a>\r\n    <a (click)=\"filter('Science')\">Science</a>\r\n    <a (click)=\"filter('Signs')\">Signs</a>\r\n    <a (click)=\"filter('Society')\">Society</a>\r\n    <a (click)=\"filter('Sports')\">Sports</a>\r\n    <a (click)=\"filter('Videogames')\">Videogames</a>\r\n    <a (click)=\"filter('Other')\">Other</a>\r\n</div>\r\n\r\n<div  *ngIf=\"loading\" class=\"spinner\">\r\n    <div class=\"cube1\"></div>\r\n    <div class=\"cube2\"></div>\r\n  </div>\r\n\r\n<div class=\"mainSidebar\"  *ngIf=\"!loading\">\r\n    <h1>Explore games</h1>\r\n    <h2>Hello there! Here you can explore a brand new world of exciting, educational and very fun kvizzes! Browse through hundreds of kvizzes created by our very best users. Choose your favorite category and GET STARTED! :)</h2>\r\n    <br>\r\n    \r\n\r\n\r\n\r\n    <table id=\"TM\">\r\n        <tr>\r\n            <th>\r\n                Quiz Name\r\n            </th>\r\n \r\n            <th>\r\n                Author\r\n            </th>\r\n \r\n            <th>\r\n                Category\r\n            </th>\r\n \r\n            <th>\r\n                Date Created\r\n            </th>\r\n        </tr>\r\n      \r\n \r\n      \r\n            <tr *ngFor=\"let q of filteredProducts\">\r\n                <td>\r\n                    <a (click)=\"goToQuiz(q.id)\">{{q.quizName}}</a>\r\n                </td>\r\n \r\n                <td>\r\n                    {{q.author}}\r\n                </td>\r\n \r\n                <td>\r\n                    {{q.category}}\r\n                </td>\r\n \r\n                <td>\r\n                    {{q.date}}\r\n                </td>\r\n            </tr>\r\n \r\n    </table>\r\n\r\n</div>"

/***/ }),

/***/ "./ClientApp/app/browse/browse.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var question_service_1 = __webpack_require__("./ClientApp/question/question.service.ts");
var BrowseComponent = /** @class */ (function () {
    function BrowseComponent(_QuestionService) {
        this._QuestionService = _QuestionService;
        this.loading = true;
        this._listFilter = "animals";
    }
    Object.defineProperty(BrowseComponent.prototype, "listFilter", {
        get: function () {
            return this._listFilter;
        },
        set: function (value) {
            this._listFilter = value;
            this.filteredProducts = this.listFilter ? this.performFilter(this.listFilter) : this.quizzes;
        },
        enumerable: true,
        configurable: true
    });
    BrowseComponent.prototype.performFilter = function (filterBy) {
        filterBy = filterBy.toLocaleLowerCase();
        return this.quizzes.filter(function (quiz) {
            return quiz.category.toLocaleLowerCase().indexOf(filterBy) !== -1;
        });
    };
    BrowseComponent.prototype.filter = function (filterby) {
        this.listFilter = filterby;
    };
    BrowseComponent.prototype.goToQuiz = function (id) {
        console.log("id: " + id);
    };
    BrowseComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._QuestionService.getQuizzes().subscribe(function (quiz) {
            _this.quizzes = quiz;
            _this.filteredProducts = _this.quizzes;
            _this.loading = false;
            console.log("quizzes: ");
            console.log(_this.quizzes);
            console.log("END");
        });
    };
    BrowseComponent = __decorate([
        core_1.Component({
            selector: 'app-browse',
            template: __webpack_require__("./ClientApp/app/browse/browse.component.html"),
            styles: [__webpack_require__("./ClientApp/app/browse/browse.component.css")]
        }),
        __metadata("design:paramtypes", [question_service_1.QuestionService])
    ], BrowseComponent);
    return BrowseComponent;
}());
exports.BrowseComponent = BrowseComponent;


/***/ }),

/***/ "./ClientApp/app/not-found.component.css":
/***/ (function(module, exports) {

module.exports = ".spinner {\r\n    margin:  300px auto 300px auto;\r\n    width: 40px;\r\n    height: 40px;\r\n    position: relative;\r\n  }\r\n  \r\n  .cube1, .cube2 {\r\n    background-color: #333;\r\n    width: 15px;\r\n    height: 15px;\r\n    position: absolute;\r\n    top: 0;\r\n    left: 0;\r\n    \r\n    -webkit-animation: sk-cubemove 1.8s infinite ease-in-out;\r\n    animation: sk-cubemove 1.8s infinite ease-in-out;\r\n  }\r\n  \r\n  .cube2 {\r\n    -webkit-animation-delay: -0.9s;\r\n    animation-delay: -0.9s;\r\n  }\r\n  \r\n  @-webkit-keyframes sk-cubemove {\r\n    25% { -webkit-transform: translateX(42px) rotate(-90deg) scale(0.5) }\r\n    50% { -webkit-transform: translateX(42px) translateY(42px) rotate(-180deg) }\r\n    75% { -webkit-transform: translateX(0px) translateY(42px) rotate(-270deg) scale(0.5) }\r\n    100% { -webkit-transform: rotate(-360deg) }\r\n  }\r\n  \r\n  @keyframes sk-cubemove {\r\n    25% { \r\n      transform: translateX(42px) rotate(-90deg) scale(0.5);\r\n      -webkit-transform: translateX(42px) rotate(-90deg) scale(0.5);\r\n    } 50% { \r\n      transform: translateX(42px) translateY(42px) rotate(-179deg);\r\n      -webkit-transform: translateX(42px) translateY(42px) rotate(-179deg);\r\n    } 50.1% { \r\n      transform: translateX(42px) translateY(42px) rotate(-180deg);\r\n      -webkit-transform: translateX(42px) translateY(42px) rotate(-180deg);\r\n    } 75% { \r\n      transform: translateX(0px) translateY(42px) rotate(-270deg) scale(0.5);\r\n      -webkit-transform: translateX(0px) translateY(42px) rotate(-270deg) scale(0.5);\r\n    } 100% { \r\n      transform: rotate(-360deg);\r\n      -webkit-transform: rotate(-360deg);\r\n    }\r\n  }\r\n  \r\n  .backgroundssss{\r\n    padding-top:160px;\r\n    background: -webkit-gradient(linear, right top, left top, from(orange), to(#ea1581));\r\n    background: linear-gradient(-90deg, orange, #ea1581)\r\n  }\r\n  \r\n  .container{\r\n    background: -webkit-gradient(linear, right top, left top, from(orange), to(#ea1581)) !important;\r\n    background: linear-gradient(-90deg, orange, #ea1581) !important;\r\n  }"

/***/ }),

/***/ "./ClientApp/app/not-found.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"backgroundssss\"></div>\r\n\r\n<div class=\"container\">\r\n<div class=\"spinner\">\r\n    <div class=\"cube1\"></div>\r\n    <div class=\"cube2\"></div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./ClientApp/app/not-found.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var PageNotFoundComponent = /** @class */ (function () {
    function PageNotFoundComponent() {
    }
    PageNotFoundComponent = __decorate([
        core_1.Component({
            template: __webpack_require__("./ClientApp/app/not-found.component.html"),
            styles: [__webpack_require__("./ClientApp/app/not-found.component.css")]
        })
    ], PageNotFoundComponent);
    return PageNotFoundComponent;
}());
exports.PageNotFoundComponent = PageNotFoundComponent;


/***/ }),

/***/ "./ClientApp/app/shared/dataService.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var BehaviorSubject_1 = __webpack_require__("./node_modules/rxjs/_esm5/BehaviorSubject.js");
var DataService = /** @class */ (function () {
    function DataService() {
        this.messageSource = new BehaviorSubject_1.BehaviorSubject("default message");
        this.currentMessage = this.messageSource.asObservable();
    }
    //brukes for å sende data mellom komponenter
    DataService.prototype.changeMessage = function (message) {
        this.messageSource.next(message);
    };
    DataService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], DataService);
    return DataService;
}());
exports.DataService = DataService;


/***/ }),

/***/ "./ClientApp/app/takequiz/takequiz.component.css":
/***/ (function(module, exports) {

module.exports = ".inputGroup {\r\n  background-color: #fff;\r\n  display: block;\r\n  margin: 10px 0;\r\n  position: relative;\r\n}\r\n\r\n.inputGroup label {\r\n  width: 100%;\r\n  display: block;\r\n  text-align: left;\r\n  color: #3C454C;\r\n  cursor: pointer;\r\n  position: relative;\r\n  z-index: 2;\r\n  -webkit-transition: color 200ms ease-in;\r\n  transition: color 200ms ease-in;\r\n  overflow: hidden;\r\n}\r\n\r\n.inputGroup label:before {\r\n  width: 10px;\r\n  height: 10px;\r\n  border-radius: 50%;\r\n  content: '';\r\n  background-color: #5562eb;\r\n  position: absolute;\r\n  left: 50%;\r\n  top: 50%;\r\n  -webkit-transform: translate(-50%, -50%) scale3d(1, 1, 1);\r\n          transform: translate(-50%, -50%) scale3d(1, 1, 1);\r\n  -webkit-transition: all 300ms cubic-bezier(0.4, 0, 0.2, 1);\r\n  transition: all 300ms cubic-bezier(0.4, 0, 0.2, 1);\r\n  opacity: 0;\r\n  z-index: -1;\r\n}\r\n\r\n.inputGroup label:after {\r\n  width: 32px;\r\n  height: 32px;\r\n  content: '';\r\n  border: 2px solid #D1D7DC;\r\n  background-color: #fff;\r\n  background-image: url(\"data:image/svg+xml,%3Csvg width='32' height='32' viewBox='0 0 32 32' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M5.414 11L4 12.414l5.414 5.414L20.828 6.414 19.414 5l-10 10z' fill='%23fff' fill-rule='nonzero'/%3E%3C/svg%3E \");\r\n  background-repeat: no-repeat;\r\n  background-position: 2px 3px;\r\n  border-radius: 50%;\r\n  z-index: 2;\r\n  position: absolute;\r\n  right: 30px;\r\n  top: 50%;\r\n  -webkit-transform: translateY(-50%);\r\n          transform: translateY(-50%);\r\n  cursor: pointer;\r\n  -webkit-transition: all 200ms ease-in;\r\n  transition: all 200ms ease-in;\r\n}\r\n\r\n.inputGroup input:checked~label {\r\n  color: #fff;\r\n}\r\n\r\n.inputGroup input:checked~label:before {\r\n  -webkit-transform: translate(-50%, -50%) scale3d(56, 56, 1);\r\n          transform: translate(-50%, -50%) scale3d(56, 56, 1);\r\n  opacity: 1;\r\n}\r\n\r\n.inputGroup input:checked~label:after {\r\n  background-color: #54E0C7;\r\n  border-color: #54E0C7;\r\n}\r\n\r\n.inputGroup input {\r\n  width: 32px;\r\n  height: 32px;\r\n  z-index: 2;\r\n  position: absolute;\r\n  right: 30px;\r\n  top: 50%;\r\n  -webkit-transform: translateY(-50%);\r\n          transform: translateY(-50%);\r\n  cursor: pointer;\r\n  visibility: hidden;\r\n}\r\n\r\n.form {\r\n  padding: 0 16px;\r\n  max-width: 550px;\r\n  margin: 50px auto;\r\n  font-size: 18px;\r\n  font-weight: 600;\r\n  line-height: 36px;\r\n  background: -webkit-gradient(linear, right top, left top, from(orange), to(rgb(255, 123, 0)));\r\n  background: linear-gradient(-90deg, orange, rgb(255, 123, 0));\r\n}\r\n\r\nbody {\r\n  background-color: #D1D7DC;\r\n  font-family: 'Fira Sans', sans-serif;\r\n}\r\n\r\n*,\r\n*::before,\r\n*::after {\r\n  -webkit-box-sizing: inherit;\r\n          box-sizing: inherit;\r\n}\r\n\r\nhtml {\r\n  -webkit-box-sizing: border-box;\r\n          box-sizing: border-box;\r\n}\r\n\r\ncode {\r\n  background-color: #9AA3AC;\r\n  padding: 0 8px;\r\n}\r\n\r\n.spinner {\r\n  margin:  300px auto 300px auto;\r\n  width: 40px;\r\n  height: 40px;\r\n  position: relative;\r\n}\r\n\r\n.cube1, .cube2 {\r\n  background-color: #333;\r\n  width: 15px;\r\n  height: 15px;\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0;\r\n  \r\n  -webkit-animation: sk-cubemove 1.8s infinite ease-in-out;\r\n  animation: sk-cubemove 1.8s infinite ease-in-out;\r\n}\r\n\r\n.cube2 {\r\n  -webkit-animation-delay: -0.9s;\r\n  animation-delay: -0.9s;\r\n}\r\n\r\n@-webkit-keyframes sk-cubemove {\r\n  25% { -webkit-transform: translateX(42px) rotate(-90deg) scale(0.5) }\r\n  50% { -webkit-transform: translateX(42px) translateY(42px) rotate(-180deg) }\r\n  75% { -webkit-transform: translateX(0px) translateY(42px) rotate(-270deg) scale(0.5) }\r\n  100% { -webkit-transform: rotate(-360deg) }\r\n}\r\n\r\n@keyframes sk-cubemove {\r\n  25% { \r\n    transform: translateX(42px) rotate(-90deg) scale(0.5);\r\n    -webkit-transform: translateX(42px) rotate(-90deg) scale(0.5);\r\n  } 50% { \r\n    transform: translateX(42px) translateY(42px) rotate(-179deg);\r\n    -webkit-transform: translateX(42px) translateY(42px) rotate(-179deg);\r\n  } 50.1% { \r\n    transform: translateX(42px) translateY(42px) rotate(-180deg);\r\n    -webkit-transform: translateX(42px) translateY(42px) rotate(-180deg);\r\n  } 75% { \r\n    transform: translateX(0px) translateY(42px) rotate(-270deg) scale(0.5);\r\n    -webkit-transform: translateX(0px) translateY(42px) rotate(-270deg) scale(0.5);\r\n  } 100% { \r\n    transform: rotate(-360deg);\r\n    -webkit-transform: rotate(-360deg);\r\n  }\r\n}\r\n\r\np{\r\n  color: black;\r\n}\r\n\r\n.summary {\r\n  padding: 0 16px;\r\n  max-width: 550px;\r\n  margin: 50px auto;\r\n  font-size: 18px;\r\n  font-weight: 600;\r\n  line-height: 36px;\r\n  background: -webkit-gradient(linear, right top, left top, from(orange), to(rgb(255, 123, 0)));\r\n  background: linear-gradient(-90deg, orange, rgb(255, 123, 0));\r\n}\r\n\r\nbody .container{\r\n  background: -webkit-gradient(linear, right top, left top, from(orange), to(#ea1581)) !important;\r\n  background: linear-gradient(-90deg, orange, #ea1581) !important;\r\n\r\n}"

/***/ }),

/***/ "./ClientApp/app/takequiz/takequiz.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- \r\n<p> TAKE QUIZ HERE</p>\r\n\r\n<div> JEG ELSKER QUIZ: <span>{{quiz?.quizInfo}}</span></div>\r\n -->\r\n\r\n<div class=\"container\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12\" style=\"margin: 75px\"></div>\r\n    <div class=\"col-sm-4\"></div>\r\n    <div class=\"col-sm-4\">\r\n\r\n      <!-- <div style=\"margin: 140px\"></div> -->\r\n        <div  *ngIf=\"loading && !showSummary\" class=\"spinner\">\r\n            <div class=\"cube1\"></div>\r\n            <div class=\"cube2\"></div>\r\n          </div>\r\n\r\n      <form class=\"form\" *ngIf=\"!loading && !showSummary\">\r\n\r\n        <h2>{{currentQuestion?.question}}</h2>\r\n        <div *ngFor=\"let item of getSizeofAlt(currentQuestion.answers)\">\r\n        \r\n          <div class=\"inputGroup\">\r\n          <input id=\"option{{item}}\" name=\"option1\" type=\"checkbox\" [(ngModel)]=\"checkedAnswers[item]\" />\r\n          <label for=\"option{{item}}\">{{currentQuestion?.answers[item]}}</label>\r\n        </div>\r\n\r\n      </div>\r\n        <button class=\"{{buttonStyle}}\" (click)='nextQuestion()' style=\"float: right\">{{nextQuestionButton}}</button>\r\n\r\n\r\n\r\n        <!--  <h2>Radio Buttons</h2>\r\n                <div class=\"inputGroup\">\r\n                  <input id=\"radio1\" name=\"radio\" type=\"radio\"/>\r\n                  <label for=\"radio1\">Yes</label>\r\n                </div>\r\n                <div class=\"inputGroup\">\r\n                  <input id=\"radio2\" name=\"radio\" type=\"radio\"/>\r\n                  <label for=\"radio2\">No</label>\r\n                </div> -->\r\n\r\n        <h3 style=\"visibility: hidden;\"> spacing</h3>\r\n        <h3 style=\"visibility: hidden;\"> spacing</h3>\r\n      </form>\r\n\r\n      <div class=\"summary\" *ngIf=showSummary>\r\n        <p>You scored {{score}} points with {{correctAnswers}} correct answers and {{wrongAnswers}} wrong answers. Good job! </p>\r\n\r\n        <button class=\"btn btn-success\" (click)=\"returnToHomepage()\"  style=\"float: right\">Finish</button>\r\n\r\n        <h3 style=\"visibility: hidden;\"> spacing</h3>\r\n        <h3 style=\"visibility: hidden;\"> spacing</h3>\r\n      </div>\r\n\r\n    </div>\r\n    <div class=\"col-sm-4\"></div>\r\n    <div class=\"col-sm-12\"></div>\r\n  </div>\r\n\r\n  \r\n</div>\r\n"

/***/ }),

/***/ "./ClientApp/app/takequiz/takequiz.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var dataService_1 = __webpack_require__("./ClientApp/app/shared/dataService.ts");
var question_service_1 = __webpack_require__("./ClientApp/question/question.service.ts");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var TakequizComponent = /** @class */ (function () {
    function TakequizComponent(elementRef, data, _QuestionService, router) {
        this.elementRef = elementRef;
        this.data = data;
        this._QuestionService = _QuestionService;
        this.router = router;
        this.questionHead = 0;
        this.checkedAnswers = ["", "", "", ""];
        this.score = 0;
        this.correctAnswers = 0;
        this.wrongAnswers = 0;
        this.nextQuestionButton = "Next Question";
        this.buttonStyle = "btn btn-info";
        this.loading = true;
        this.showSummary = false;
    }
    TakequizComponent.prototype.ngOnInit = function () {
        var _this = this;
        //Getting quizID
        this.data.currentMessage.subscribe(function (message) { return _this.message = message; });
        //Gets quiz
        this._QuestionService.getQuiz(this.message).subscribe(function (quiz) {
            _this.quiz = quiz;
            _this.loading = false;
            _this.nextQuestion();
        });
    };
    TakequizComponent.prototype.checkAnswer = function () {
        var tempscore = 0;
        for (var i = 0; i < this.currentQuestion.answers.length; i++) {
            if (this.checkedAnswers[i] && this.currentQuestion.rightAnswers[i] == "true") {
                tempscore++;
                this.correctAnswers++;
            }
            if (this.checkedAnswers[i] && this.currentQuestion.rightAnswers[i] == "false") {
                tempscore--;
                this.wrongAnswers++;
            }
        }
        if (tempscore < 0)
            tempscore = 0;
        this.score += tempscore;
        for (var i_1 in this.checkedAnswers)
            this.checkedAnswers[i_1] = "";
    };
    TakequizComponent.prototype.nextQuestion = function () {
        if (this.currentQuestion)
            this.checkAnswer();
        if (this.quiz.questions[this.questionHead]) {
            this.currentQuestion = this.quiz.questions[this.questionHead];
            this.questionHead++;
            if (!this.quiz.questions[this.questionHead]) {
                this.nextQuestionButton = "Submit";
                this.buttonStyle = "btn btn-success";
            }
        }
        else
            this.Summary();
    };
    TakequizComponent.prototype.Summary = function () {
        this.quiz.quizNum++;
        this._QuestionService.editQuiz(this.message, this.quiz.quizNum);
        this.showSummary = true;
    };
    TakequizComponent.prototype.returnToHomepage = function () {
        //Burde være en bedre måte å navigere tilbake til hjemmesiden?
        this.router.navigateByUrl("/Home");
        window.location.reload();
    };
    TakequizComponent.prototype.getSizeofAlt = function (answers) {
        var x = answers.length;
        var items = [];
        for (var i = 0; i < x; i++) {
            items.push(i);
        }
        return items;
    };
    TakequizComponent = __decorate([
        core_1.Component({
            selector: 'app-takequiz',
            template: __webpack_require__("./ClientApp/app/takequiz/takequiz.component.html"),
            styles: [__webpack_require__("./ClientApp/app/takequiz/takequiz.component.css")]
        }),
        __metadata("design:paramtypes", [core_1.ElementRef, dataService_1.DataService, question_service_1.QuestionService, router_1.Router])
    ], TakequizComponent);
    return TakequizComponent;
}());
exports.TakequizComponent = TakequizComponent;


/***/ }),

/***/ "./ClientApp/environments/environment.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    production: false
};


/***/ }),

/***/ "./ClientApp/main.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var platform_browser_dynamic_1 = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
var app_module_1 = __webpack_require__("./ClientApp/app/app.module.ts");
var environment_1 = __webpack_require__("./ClientApp/environments/environment.ts");
if (environment_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(app_module_1.AppModule)
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ "./ClientApp/question/question.component.css":
/***/ (function(module, exports) {

module.exports = ".transition, input[type=checkbox]:after {\r\n  -webkit-transition: all 0.25s cubic-bezier(0.75, 0.01, 0.48, 2);\r\n  transition: all 0.25s cubic-bezier(0.75, 0.01, 0.48, 2);\r\n}\r\n\r\n.bgTransition, input[type=checkbox]:before {\r\n  -webkit-transition: all 0.25s ease-in-out;\r\n  transition: all 0.25s ease-in-out;\r\n}\r\n\r\ninput[type=checkbox] {\r\n  position: relative;\r\n  -webkit-tap-highlight-color: transparent;\r\n  -webkit-touch-callout: none;\r\n  -webkit-user-select: none;\r\n  -moz-user-select: none;\r\n  -ms-user-select: none;\r\n  user-select: none;\r\n}\r\n\r\ninput[type=checkbox]:before, input[type=checkbox]:after {\r\n  content: \"\";\r\n  position: absolute;\r\n}\r\n\r\ninput[type=checkbox]:before {\r\n  height: 100%;\r\n}\r\n\r\ninput[type=checkbox][type=checkbox] {\r\n  border-radius: 30px;\r\n}\r\n\r\ninput[type=checkbox][type=checkbox]:before {\r\n  width: 100%;\r\n/*   border-radius: 30px; */\r\n  background-color: #d9534f;\r\n  -webkit-box-shadow: 0 0 0 1px #2c3f52;\r\n          box-shadow: 0 0 0 1px #2c3f52;\r\n}\r\n\r\ninput[type=checkbox][type=checkbox]:after {\r\n  width: 100%;\r\n  height: 100%;\r\n  border-radius: 30px;\r\n  background-color: #85ff7a;\r\n  -webkit-transform: scale(0, 0);\r\n  transform: scale(0, 0);\r\n}\r\n\r\ninput[type=checkbox][type=checkbox]:checked:after {\r\n  -webkit-transform: scale(0.75, 0.75);\r\n  transform: scale(0.75, 0.75);\r\n}\r\n\r\ninput[type=checkbox][type=checkbox]:checked:before {\r\n  background-color: #15202a;\r\n  -webkit-box-shadow: 0 0 0 1px #15202a;\r\n          box-shadow: 0 0 0 1px #15202a;\r\n}\r\n\r\nhtml {\r\n  background-color: #15202a;\r\n}\r\n\r\ninput[type=checkbox] {\r\n  cursor: pointer;\r\n  width: 30px;\r\n  height: 30px;\r\n}\r\n\r\n.check {\r\n  float: right\r\n}\r\n\r\n.col-md-4.control-label{\r\n  color:black;\r\n}\r\n\r\n.backgroundssss{\r\n  padding-top:160px;\r\n  background: -webkit-gradient(linear, right top, left top, from(orange), to(#ea1581));\r\n  background: linear-gradient(-90deg, orange, #ea1581)\r\n}\r\n\r\n"

/***/ }),

/***/ "./ClientApp/question/question.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"backgroundssss\"></div>\r\n\r\n<form class=\"form-horizontal\" style=\"background: linear-gradient(-90deg, orange, #ea1581)\" >\r\n    <!-- Quiz Navn -->\r\n    <div class=\"form-group\">\r\n        <div class=\"row\">\r\n            <label class=\"col-md-4 control-label\" for=\"textarea\">Quiz Name</label>\r\n            <div class=\"col-md-4\">\r\n                <input [(ngModel)]=\"quiz.QuizName\" name=\"quizName\" type=\"text\" placeholder=\"Type a quiz name\" class=\"form-control input-md\"\r\n                    required>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <!-- Quiz Info -->\r\n    <div class=\"form-group\">\r\n        <label class=\"col-md-4 control-label\" for=\"textarea\">Quiz Info</label>\r\n        <div class=\"col-md-4\">\r\n            <input [(ngModel)]=\"quiz.QuizInfo\" name=\"quizInfo\" type=\"text\" placeholder=\"Type a quiz description\" class=\"form-control input-md\"\r\n                required>\r\n        </div>\r\n    </div>\r\n\r\n    <!-- author -->\r\n    <div class=\"form-group\">\r\n        <label class=\"col-md-4 control-label\" for=\"textarea\">Author</label>\r\n        <div class=\"col-md-4\">\r\n            <input [(ngModel)]=\"quiz.Author\" name=\"Author\" type=\"text\" placeholder=\"Type your name\" class=\"form-control input-md\" required>\r\n        </div>\r\n    </div>\r\n\r\n    <!-- Basic Dropdown menu -->\r\n    <div class=\"form-group\">\r\n        <label class=\"col-md-4 control-label\" for=\"selectbasic\">Category</label>\r\n        <div class=\"col-md-4\">\r\n            <select id=\"selectbasic\" name=\"selectbasic\" class=\"form-control\" [(ngModel)]=\"selectedCategory\">\r\n                <option *ngFor=\"let category of categories\" [value]=\"category\">{{category}}</option>\r\n            </select>\r\n        </div>\r\n    </div>\r\n\r\n    <div *ngFor=\"let question of questions; let i=index\" [ngStyle]=\"qStyle(i)\">\r\n            <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\" style=\"visibility: hidden;\"> SPACING </div>\r\n        </div>\r\n        </div>\r\n        <!-- Spørsmål -->\r\n\r\n        <div class=\"container\" id=\"spm\">>\r\n        <div class=\"row\">\r\n            <div class=\"form-group\">\r\n                <label class=\"col-md-4 control-label\" for=\"textarea\">Question</label>\r\n                <div class=\"col-md-3\">\r\n                    <textarea [(ngModel)]='question.Question' [name]=\"genQueName(i)\" class=\"form-control\" placeholder=\"Type a question\"></textarea>\r\n                </div>\r\n                <div class=\"col-md-1\">\r\n                    <button class=\"btn btn-danger\" (click)=\"deleteQuestion(i)\">Delete Question</button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        </div>\r\n\r\n\r\n        <!-- Svar Alternativ + checkbox -->\r\n        <div *ngFor=\"let item of getSizeofAlt(question.answers);\">\r\n                <div class=\"container\">\r\n            <div class=\"row\">\r\n                <div class=\"form-group\">\r\n                    <div class=\"col-md-3\"></div>\r\n                    <div class=\"col-md-1\">\r\n                        <div class=\"check\">\r\n                            <span>\r\n                                <input type=\"checkbox\" class=\"form-control\" [(ngModel)]=\"question.rightAnswers[item]\" [name]=\"genRightName(i,item)\">\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-md-3\">\r\n                        <div class=\"form-group\" dnd-sortable-container [sortableData]=\"question.answers\">\r\n                            <input [(ngModel)]='question.answers[item]' [name]=\"genAnsName(i,item)\" class=\"form-control\" type=\"text\" placeholder=\"Type an answer\"\r\n                            dnd-sortable [sortableIndex]=\"item\">\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-md-1\">\r\n                        <button class=\"btn btn-danger\" (click)=\"deleteAnswer(i,item)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                        </button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"container\">\r\n        <div class=\"row\">\r\n        <div class=\"col-md-4\"></div>\r\n        <div class=\"col-md-4\">\r\n            <button class=\"btn btn-info\" (click)=\"newAnswer(i)\">New Answer</button>\r\n        </div>\r\n        </div>\r\n    </div>\r\n\r\n    </div>\r\n\r\n    <div class=\"container\">\r\n    <div class=\"row\" [ngStyle]=\"qStyle(questions.length-1)\">\r\n        <div class=\"col-md-4\"></div>\r\n        <div class=\"col-md-4\">\r\n            <button class=\"btn btn-success\" (click)='saveQuiz()' style=\"float: right\">Save Quiz</button>\r\n            <button class=\"btn btn-info\" (click)='newQuestion()' style=\"float: right\">New Question</button>\r\n        </div>\r\n    </div>\r\n    </div>\r\n\r\n\r\n</form>"

/***/ }),

/***/ "./ClientApp/question/question.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var question_service_1 = __webpack_require__("./ClientApp/question/question.service.ts");
var question_1 = __webpack_require__("./ClientApp/question/question.ts");
var quiz_1 = __webpack_require__("./ClientApp/question/quiz.ts");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var common_1 = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var Question = /** @class */ (function () {
    function Question(_QuestionService, router, datePipe) {
        this._QuestionService = _QuestionService;
        this.router = router;
        this.datePipe = datePipe;
        this.questions = [];
        this.categories = new Array("Sports", "Geography", "Politics", "Videogames", "Film/TV", "Food and drink", "Animals", "History", "Science", "Religion", "Music", "Culture", "Society", "Languages", "Motors", "Signs", "People", "Other");
        this.questionStyle = [{ 'background': 'linear-gradient(-90deg, red, orange)' }, { 'background': 'linear-gradient(-90deg, orange, yellow)' }, { 'background': 'linear-gradient(-90deg, yellow, green)' }, { 'background': 'linear-gradient(-90deg, green, blue)' }, { 'background': 'linear-gradient(-90deg, blue, purple)' }, { 'background': 'linear-gradient(-90deg, purple, pink)' }];
        this.questions.push(new question_1.questionModel());
        this.quiz = new quiz_1.quizModel();
    }
    Question.prototype.saveQuiz = function () {
        this.quiz.questions = this.questions; //Add questions
        //Add date
        var date = new Date();
        this.quiz.date = this.datePipe.transform(date, "yyyy-MM-dd");
        //Add category
        if (this.selectedCategory == null) {
            this.quiz.category = "Other";
        }
        else
            this.quiz.category = this.selectedCategory;
        this._QuestionService.saveQuiz(this.quiz);
    };
    Question.prototype.newQuestion = function () {
        this.questions.push(new question_1.questionModel());
    };
    Question.prototype.deleteQuestion = function (idx) {
        this.questions.splice(idx, 1);
    };
    Question.prototype.newAnswer = function (idx) {
        this.questions[idx].answers.push("");
        this.questions[idx].rightAnswers.push(null);
    };
    Question.prototype.deleteAnswer = function (qidx, aidx) {
        this.questions[qidx].answers.splice(aidx, 1);
        this.questions[qidx].rightAnswers.splice(aidx, 1);
    };
    Question.prototype.genAnsName = function (idx, idx2) {
        return "answer" + idx + idx2;
    };
    Question.prototype.genRightName = function (idx, idx2) {
        return "rightAnswer" + idx + idx2;
    };
    Question.prototype.genQueName = function (idx) {
        return "question" + idx;
    };
    Question.prototype.ngOnInit = function () {
        this.categories.sort();
    };
    Question.prototype.qStyle = function (idx) {
        while (idx > this.questionStyle.length - 1)
            idx -= this.questionStyle.length;
        return this.questionStyle[idx];
    };
    Question.prototype.getSizeofAlt = function (answers) {
        var x = answers.length;
        var items = [];
        for (var i = 0; i < x; i++) {
            items.push(i);
        }
        return items;
    };
    Question = __decorate([
        core_1.Component({
            selector: 'pm-question',
            template: __webpack_require__("./ClientApp/question/question.component.html"),
            styles: [__webpack_require__("./ClientApp/question/question.component.css")],
            providers: [question_1.questionModel]
        }),
        __metadata("design:paramtypes", [question_service_1.QuestionService, router_1.Router, common_1.DatePipe])
    ], Question);
    return Question;
}());
exports.Question = Question;


/***/ }),

/***/ "./ClientApp/question/question.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var Observable_1 = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");
__webpack_require__("./node_modules/rxjs/_esm5/add/operator/catch.js");
__webpack_require__("./node_modules/rxjs/_esm5/add/operator/do.js");
__webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var QuestionService = /** @class */ (function () {
    function QuestionService(_http, router) {
        this._http = _http;
        this.router = router;
        this._quizUrl = "/api/Quiz";
    }
    //Denne funksjonen er ikke testa / vet ikke om den virker
    QuestionService.prototype.getQuizzes = function () {
        return this._http.get(this._quizUrl + "/GetAll")
            .catch(this.handleError);
    };
    QuestionService.prototype.getQuiz = function (id) {
        var searchparams = new http_1.HttpParams().set("id", id);
        return this._http.get(this._quizUrl + "/GetById", { params: searchparams })
            .do(function (data) { return console.log("Quizzen her: " + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    /*         .do(data => console.log('Quiz-info hentet: ' + JSON.stringify(data))) */
    QuestionService.prototype.editQuiz = function (id, quizNum) {
        this._http.put(this._quizUrl + "/addNumberOfQuizTaken?id=" + id + "&quizNum=" + quizNum, quizNum)
            .subscribe(function (data) {
            console.log("Http result:");
            console.log(data);
        }, function (error) {
            console.log("Http error :");
            console.log(error);
        });
    };
    QuestionService.prototype.saveQuiz = function (quiz) {
        var _this = this;
        this._http.post(this._quizUrl, quiz)
            .subscribe(function (data) {
            console.log("Http result:");
            console.log(data);
            _this.router.navigateByUrl("/Home");
            window.location.reload();
        }, function (error) {
            console.log("Http error :");
            console.log(error);
            _this.router.navigateByUrl("/Home");
            window.location.reload();
        });
    };
    QuestionService.prototype.handleError = function (err) {
        console.log(err.message);
        return Observable_1.Observable.throw(err.message);
    };
    QuestionService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient, router_1.Router])
    ], QuestionService);
    return QuestionService;
}());
exports.QuestionService = QuestionService;


/***/ }),

/***/ "./ClientApp/question/question.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var questionModel = /** @class */ (function () {
    function questionModel() {
        this.answers = new Array(2);
        this.rightAnswers = new Array(2);
    }
    Object.defineProperty(questionModel.prototype, "Question", {
        get: function () {
            return this.question;
        },
        set: function (value) {
            this.question = value;
        },
        enumerable: true,
        configurable: true
    });
    return questionModel;
}());
exports.questionModel = questionModel;


/***/ }),

/***/ "./ClientApp/question/quiz.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var quizModel = /** @class */ (function () {
    function quizModel() {
        this.quizNum = 0;
    }
    Object.defineProperty(quizModel.prototype, "QuizName", {
        get: function () {
            return this.quizName;
        },
        set: function (value) {
            this.quizName = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(quizModel.prototype, "QuizInfo", {
        get: function () {
            return this.quizInfo;
        },
        set: function (value) {
            this.quizInfo = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(quizModel.prototype, "Author", {
        get: function () {
            return this.author;
        },
        set: function (value) {
            this.author = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(quizModel.prototype, "Category", {
        get: function () {
            return this.category;
        },
        set: function (value) {
            this.category = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(quizModel.prototype, "QuizNum", {
        get: function () {
            return this.quizNum;
        },
        set: function (value) {
            this.quizNum = value;
        },
        enumerable: true,
        configurable: true
    });
    return quizModel;
}());
exports.quizModel = quizModel;


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./ClientApp/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map