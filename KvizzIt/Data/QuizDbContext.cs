using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;
using KvizzIt.Models;
using MongoDB.Bson;

public class QuizDbContext
{
    public IMongoDatabase Database;

    public QuizDbContext()
    {
        //string connectionString = "mongodb://localhost:27017";
        string connectionString = "mongodb+srv://root:bW975FiV88e86WHb@kvizzdb-wzmtg.mongodb.net/test";

        var client = new MongoClient(connectionString);
        Database = client.GetDatabase("quizzes");
    }

        
    //public DbSet<QuizModel> Quizzes { get; set; }

    public IMongoCollection<QuizModel> Quizzes => Database.GetCollection<QuizModel>("quizzes");

    
    public QuizModel Get(string id)
        {
            return this.Quizzes.Find(new BsonDocument { { "_id", new ObjectId(id) } }).FirstAsync().Result;
        }

    
}

