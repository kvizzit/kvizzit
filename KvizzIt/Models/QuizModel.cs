using System.ComponentModel;
using MongoDB.Bson;

namespace KvizzIt.Models
{
    public class QuizModel
    {
        public QuizModel(){}


        public QuizModel(QuizModel GeneralQuizInfo ){
            quizName = GeneralQuizInfo.quizName;
            quizInfo = GeneralQuizInfo.quizInfo;
            author = GeneralQuizInfo.author;
            category = GeneralQuizInfo.category;
            date = GeneralQuizInfo.date;
            imgUrl = GeneralQuizInfo.imgUrl;
            questions = GeneralQuizInfo.questions;
            quizNum = GeneralQuizInfo.quizNum;

        }

        public ObjectId Id{get; set;}

        [DisplayName("Quiz Name")]
        public string quizName{get; set;}

        [DisplayName("Quiz Info")]
        public string quizInfo{get; set;}

        [DisplayName("Author")]
        public string author{get; set;}

        [DisplayName("Category")]
        public string category{get; set;}

        [DisplayName("Date")]
        public string date{get; set;}

        [DisplayName("Image")]
        public string imgUrl{get; set;}
        [DisplayName("Number of times quiz taken")]
        public int quizNum {get; set;}
        public questionModel[] questions {get; set;}

    }
}