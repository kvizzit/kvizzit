using MongoDB.Bson;

namespace KvizzIt.Models{
    public class questionModel{
        public questionModel(){}
        public questionModel(questionModel q){
            question = q.question;
            answers = q.answers;
            rightAnswers = q.rightAnswers; 
        }


        
        public string question {get; set;}
        public string[] answers {get; set; }
         public string[] rightAnswers {get; set;}
    }
}