﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using KvizzIt.Models;
using MongoDB.Driver;
using MongoDB.Bson;
using static Microsoft.AspNetCore.Hosting.Internal.HostingApplication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using static KvizzIt.Models.QuizModel;



namespace KvizzIt.Controllers
{
    public class HomeController : Controller
    {

        public readonly QuizDbContext _context = new QuizDbContext();

        public IActionResult Index()
        {
            var quizzes = _context.Quizzes.Find(_ => true).ToList();
            return View(quizzes);
        }

         public IActionResult Browse()
        {
            return View();
        }

        public IActionResult CreateQuiz()
        {
            return View();
        }

         public ActionResult QuizInfo(string Id)
        {
            QuizModel x = _context.Get(Id);
            return View(_context.Get(Id));
        }

        public IActionResult TakeQuiz(string Id)
        {
            ViewData["Id"] = Id;
            return View();
        }

        public IActionResult CreateQuiz2()
        {
            return View();
        }
        

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Post()
        {
            
            return RedirectToAction("Index");
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult Test()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }
    }
}
