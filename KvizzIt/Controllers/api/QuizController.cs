using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using KvizzIt.Models;
using Microsoft.Extensions.Logging;
using KvizzIt.Core;
using MongoDB.Driver;
using MongoDB.Bson;


namespace KvizzIt.Controllers
{
    [Route("api/[Controller]")]
    public class QuizController : Controller
    {

        private readonly ILogger _logger;
        public readonly QuizDbContext _context = new QuizDbContext();

        public QuizController(QuizDbContext context, ILogger<QuizController> logger)
        {
            _logger = logger;
            _context = context;
        }

        [HttpPost]
        public IActionResult Post([FromBody] QuizModel quiz)
        {

            var Quiz = new QuizModel(quiz);

            //Change values in rightanswer(from null to false)
            for (int i = 0; i < Quiz.questions.Length; i++)
            {
                for (int y = 0; y < Quiz.questions[i].rightAnswers.Length; y++)
                {
                    if (Quiz.questions[i].rightAnswers[y] == null) Quiz.questions[i].rightAnswers[y] = "false";
                }
            }
            //Save quiz to database
            _context.Quizzes.InsertOne(Quiz);
            return Ok();
        }

       /*  [HttpPut]
        [Route("addNumberOfQuizTaken")]
        public IActionResult Put(string id, string quizNum){
 
            ObjectId xx = new ObjectId(id);
             
            _context.Quizzes.FindOneAndUpdate(Builders<QuizModel>.Filter.Eq("_id", xx),
            Builders<QuizModel>.Update.Set("quizNum", quizNum)
            ); 

>>>>>>> master
            return Ok();
        }
 */
        [HttpPut]
        [Route("addNumberOfQuizTaken")]
        public IActionResult Put([FromQuery]string id,[FromQuery]string quizNum){

             
             _context.Quizzes.FindOneAndUpdate(new BsonDocument { { "_id", new ObjectId(id) } },
            Builders<QuizModel>.Update.Set("quizNum", quizNum)
            ); 
 
            return Ok();
        }

        [HttpGet]
        [Route("GetById")]
        public IActionResult Get(string id)
        {
            try
            {
                return Ok(_context.Quizzes.Find(new BsonDocument { { "_id", new ObjectId(id) } }).FirstAsync().Result);
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpGet]
        [Route("GetAll")]
        public IActionResult Get()
        {
            try
            {
                return Ok(_context.Quizzes.Find(_ => true).ToList().ToArray());
            }
            catch
            {
                return NotFound();
            }
        }

    }
}