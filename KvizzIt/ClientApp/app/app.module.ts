import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { DndModule } from 'ng2-dnd';

import { RouterModule, Routes } from '@angular/router';
import { APP_BASE_HREF, DatePipe } from '@angular/common';

import { AppComponent } from './app.component';
import { DataService } from "./shared/dataService"
import { Question } from '../question/question.component';
import { QuestionService } from '../question/question.service';
import { TakequizComponent } from './takequiz/takequiz.component';
import { PageNotFoundComponent} from './not-found.component';
import { BrowseComponent } from './browse/browse.component';


const appRoutes: Routes = [
  { path: 'Home/CreateQuiz', component: Question },
  { path: 'Home/TakeQuiz/:id', component: TakequizComponent},
  { path: 'Home/Browse', component: BrowseComponent},
  { path: '**', component: PageNotFoundComponent}
]


@NgModule({
  declarations: [
    AppComponent,
    Question,
    TakequizComponent,
    PageNotFoundComponent,
    BrowseComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,

    DndModule.forRoot(),

    RouterModule.forRoot(
      appRoutes
/*       { enableTracing: true} // logger alle interne routing events til consollen */
    )

  ],
  providers: [
    DataService,
    QuestionService,
    DatePipe,
    {provide: APP_BASE_HREF, useValue: "/"}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
