import { Component, OnInit, ElementRef } from '@angular/core';
import { DataService } from "../shared/dataService";
import { quizModel } from '../../question/quiz';
import { QuestionService } from '../../question/question.service';
import { questionModel } from '../../question/question';
import { Router } from "@angular/router";

@Component({
  selector: 'app-takequiz',
  templateUrl: 'takequiz.component.html',
  styleUrls: ['takequiz.component.css']
})
export class TakequizComponent implements OnInit {

  
  
  message: string;
  quiz: quizModel;
  currentQuestion: questionModel;
  questionHead: number = 0;
  checkedAnswers: string[] = ["", "", "", ""];
  score: number = 0;
  correctAnswers: number = 0;
  wrongAnswers: number = 0;
  nextQuestionButton: string = "Next Question";
  buttonStyle: string = "btn btn-info";
  loading: boolean = true;
  showSummary: boolean = false;
  

  constructor(private elementRef: ElementRef, private data: DataService, private _QuestionService: QuestionService, private router: Router){
  
  }

  ngOnInit() {
    //Getting quizID
    this.data.currentMessage.subscribe(message => this.message = message)
    //Gets quiz
    this._QuestionService.getQuiz(this.message).subscribe(quiz =>{
      this.quiz = quiz;
      this.loading = false;
      this.nextQuestion();
    });

  }

  checkAnswer(): void {
    let tempscore = 0;

    for(var i=0; i<this.currentQuestion.answers.length; i++){
      if(this.checkedAnswers[i] && this.currentQuestion.rightAnswers[i]=="true") {tempscore++; this.correctAnswers++;}
      if(this.checkedAnswers[i] && this.currentQuestion.rightAnswers[i]=="false") {tempscore--; this.wrongAnswers++;}
    }

    if(tempscore<0) tempscore = 0;
    this.score += tempscore;

    for(let i in this.checkedAnswers) this.checkedAnswers[i] = "";
  }

  

  nextQuestion(): void {
    if(this.currentQuestion) this.checkAnswer();
    if(this.quiz.questions[this.questionHead]){
      this.currentQuestion = this.quiz.questions[this.questionHead];
      this.questionHead++;
      if(!this.quiz.questions[this.questionHead]){
        this.nextQuestionButton = "Submit"
        this.buttonStyle = "btn btn-success"
      }
      
    }
    else this.Summary();
  }

  Summary(): void {
    this.quiz.quizNum++;
    this._QuestionService.editQuiz(this.message, this.quiz.quizNum);
    this.showSummary = true;
  }

  returnToHomepage(): void {
    
     //Burde være en bedre måte å navigere tilbake til hjemmesiden?
    this.router.navigateByUrl("/Home");
    window.location.reload(); 
  }

  getSizeofAlt(answers: string[]){
    var x = answers.length;
    var items: number[] = [];
    for(var i = 0; i < x; i++){
       
       items.push(i);
    }
    return items;
  }
}



