import { Component, OnInit } from '@angular/core';
import { quizModel } from '../../question/quiz';
import { QuestionService } from '../../question/question.service';

@Component({
  selector: 'app-browse',
  templateUrl: 'browse.component.html',
  styleUrls: ['browse.component.css']
})
export class BrowseComponent implements OnInit {


  quizzes: quizModel[];
  filteredProducts: quizModel[];
  loading: boolean = true;

  constructor(private _QuestionService: QuestionService) { }


  _listFilter: string = "animals";
  get listFilter(): string {
      return this._listFilter;
  }
  set listFilter(value: string) {
      this._listFilter = value;
      this.filteredProducts = this.listFilter ? this.performFilter(this.listFilter) : this.quizzes;
  }

  performFilter(filterBy: string): quizModel[] {
    filterBy = filterBy.toLocaleLowerCase();
    return this.quizzes.filter((quiz: quizModel) =>
          quiz.category.toLocaleLowerCase().indexOf(filterBy) !== -1);
}

filter(filterby: string){
  this.listFilter = filterby
}
  
goToQuiz(id: string) {
  console.log("id: " + id)
}


  ngOnInit() {
    this._QuestionService.getQuizzes().subscribe(quiz =>{
    this.quizzes = quiz;
    this.filteredProducts=this.quizzes;
    this.loading = false;
    console.log("quizzes: ")
    console.log(this.quizzes);
    console.log("END")
  
    
    });
  }
}
