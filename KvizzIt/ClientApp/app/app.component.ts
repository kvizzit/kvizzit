import { Component, ElementRef, OnInit } from '@angular/core';
import { DataService } from "./shared/dataService";

@Component({
  selector: 'app-root',
  templateUrl: "./app.component.html",
  styles: []
})
export class AppComponent implements OnInit {
  title = 'KvizzIt';

  constructor(private elementRef: ElementRef, private data: DataService){}

  quizId: string = this.elementRef.nativeElement.getAttribute('quizId');

  message: string;
  
  ngOnInit() {
    this.data.currentMessage.subscribe(message => this.message = message)

    this.data.changeMessage(this.quizId)

  }
}

