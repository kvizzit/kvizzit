import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class DataService {

  private messageSource = new BehaviorSubject<string>("default message");
  currentMessage = this.messageSource.asObservable();

  constructor() { }


  //brukes for å sende data mellom komponenter
  changeMessage(message: string) {
    this.messageSource.next(message)
  }
}