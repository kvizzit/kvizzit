import { questionModel } from "./question";

export class quizModel{

id: string;

quizName: string;
get QuizName(): string {
    return this.quizName;
} 
set QuizName(value: string) {
    this.quizName = value;
}


quizInfo: string;
get QuizInfo(): string {
    return this.quizInfo;
}
set QuizInfo(value: string){
    this.quizInfo = value;
}


author: string;
get Author(): string {
    return this.author;
}
set Author(value: string){
    this.author = value;
}

category: string;
get Category(): string {
    return this.category;
}
set Category(value: string){
    this.category = value;
}

quizNum: number = 0;
get QuizNum(): number {
    return this.quizNum;
}
set QuizNum(value: number){
    this.quizNum = value;
}
date: string;

imgUrl: string;

questions: questionModel[];

}