import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from "@angular/common/http"
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { quizModel } from "./quiz";
import { Question } from "./question.component";

import {FormControl, FormGroup, FormArray, Validators} from '@angular/forms';
import { questionModel } from "./question";
import { Router } from "@angular/router";


@Injectable()
export class QuestionService {
    private _quizUrl = "/api/Quiz";

    constructor(private _http: HttpClient, private router: Router) {

    }

    //Denne funksjonen er ikke testa / vet ikke om den virker
     getQuizzes(): Observable<quizModel[]> {
        return this._http.get<quizModel[]>(this._quizUrl + "/GetAll")
        .catch(this.handleError);
    } 



    getQuiz(id: string): Observable<quizModel> {

        let searchparams = new HttpParams().set("id", id);
 
        return this._http.get<quizModel>(this._quizUrl + "/GetById", {params: searchparams})
        .do(data => console.log("Quizzen her: " + JSON.stringify(data)))
        .catch(this.handleError);
        }
    
/*         .do(data => console.log('Quiz-info hentet: ' + JSON.stringify(data))) */


        editQuiz(id: string, quizNum: number){

            this._http.put<questionModel>(this._quizUrl + "/addNumberOfQuizTaken?id=" + id + "&quizNum=" + quizNum, quizNum)
            .subscribe(data =>{
                console.log("Http result:")
                console.log(data)
            },
        error=>{
            console.log("Http error :")
            console.log(error)
        });
        }

     saveQuiz(quiz: quizModel) {

        this._http.post<quizModel>(this._quizUrl, quiz)
        .subscribe(data =>{
            console.log("Http result:")
            console.log(data)
            this.router.navigateByUrl("/Home");
            window.location.reload(); 
        },
    error=>{
        console.log("Http error :")
        console.log(error)
        this.router.navigateByUrl("/Home");
        window.location.reload(); 
    });
    } 

    private handleError(err: HttpErrorResponse) {
        console.log(err.message);
        return Observable.throw(err.message);
    }


}

