import { Component, OnInit } from "@angular/core";
import { QuestionService } from "./question.service";

import { questionModel } from "./question";
import { quizModel } from "./quiz";
import { Router } from "@angular/router";

import { TestBed, async } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { DndModule } from 'ng2-dnd';
import {FormControl, FormGroup, FormArray, Validators} from '@angular/forms';
import { DatePipe } from "@angular/common";


@Component({
    selector: 'pm-question',
    templateUrl: 'question.component.html',
    styleUrls: ['question.component.css'],
    providers: [questionModel]

})

export class Question implements OnInit {

    questions: questionModel[] = [];
    quiz: quizModel;
    categories: string[] = new Array("Sports", "Geography", "Politics", "Videogames", "Film/TV", "Food and drink",
     "Animals", "History", "Science", "Religion", "Music", "Culture", "Society", "Languages", "Motors", "Signs", "People", "Other");
    selectedCategory: string;
    errorMessage: string;
    questionStyle = [{ 'background': 'linear-gradient(-90deg, red, orange)'} , { 'background': 'linear-gradient(-90deg, orange, yellow)'}, {'background': 'linear-gradient(-90deg, yellow, green)'}, { 'background': 'linear-gradient(-90deg, green, blue)'}, { 'background': 'linear-gradient(-90deg, blue, purple)'}, { 'background': 'linear-gradient(-90deg, purple, pink)'}  ];



    constructor(private _QuestionService: QuestionService, private router: Router, private datePipe: DatePipe){

        this.questions.push( new questionModel());
        this.quiz = new quizModel();
        }
     
         saveQuiz(): void {
            this.quiz.questions = this.questions; //Add questions
            //Add date
            var date = new Date();
            this.quiz.date = this.datePipe.transform(date,"yyyy-MM-dd");
     
            //Add category
            if(this.selectedCategory==null){
                 this.quiz.category = "Other";
            }
            else this.quiz.category = this.selectedCategory;
     
            this._QuestionService.saveQuiz(this.quiz);
        }
        
    newQuestion(): void {
        this.questions.push(new questionModel());
    }

    deleteQuestion(idx: number): void {
        this.questions.splice(idx, 1);
    }

    newAnswer(idx: number ): void {
        this.questions[idx].answers.push("");
        this.questions[idx].rightAnswers.push(null);
    }

    deleteAnswer(qidx: number, aidx: number){
        this.questions[qidx].answers.splice(aidx, 1);
        this.questions[qidx].rightAnswers.splice(aidx, 1);
    }

    genAnsName(idx: number, idx2: number){
        return "answer" + idx + idx2;
    }

    genRightName(idx: number, idx2: number){
        return "rightAnswer" + idx + idx2;
    }
    genQueName(idx: number){
        return "question" + idx;
    }

    ngOnInit(): void {
        this.categories.sort();
    }


    qStyle(idx : number) {
        while(idx>this.questionStyle.length-1) idx-=this.questionStyle.length;
        return this.questionStyle[idx];
    }
    getSizeofAlt(answers: string[]){
        var x = answers.length;
        var items: number[] = [];
        for(var i = 0; i < x; i++){
           
           items.push(i);
        }
        return items;
    }
}
