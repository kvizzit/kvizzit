import { NumberSymbol } from "@angular/common";

export class questionModel{
    
    question: string;
    get Question(): string {
        return this.question;
    }
    set Question(value: string) {
        this.question = value;
    } 
    
    answers: string[] = new Array(2);
    rightAnswers: string[] = new Array(2); 

}
